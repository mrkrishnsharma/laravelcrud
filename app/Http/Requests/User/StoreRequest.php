<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRequest
 * @package App\Http\Requests
 *
 * @SWG\Definition(
 *    definition="StoreRequest",
 *    required = {"first_name", "last_name", "mobile", "email", "address", "dob"},
 *    @SWG\Property(
 *        property="first_name",
 *        description="first name",
 *        type="integer"
 *    ),
 *    @SWG\Property(
 *        property="last_name",
 *        description="last name",
 *        type="string"
 *    ),
 *    @SWG\Property(
 *        property="mobile",
 *        description="mobile",
 *        type="string"
 *    ),
 *    @SWG\Property(
 *        property="email",
 *        description="email",
 *        type="string"
 *    ),
 *    @SWG\Property(
 *        property="address",
 *        description="address",
 *        type="string"
 *    ),
 *    @SWG\Property(
 *        property="dob",
 *        description="dob",
 *        type="string"
 *    )
 * )
 */

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:100',
            'last_name' => 'required|string|max:100',
            'email' => 'required|unique:users,email|email',
            'address' => 'required|string|max:100',
            'dob' => 'required|date_format:Y-m-d|before:today',
            'mobile' => 'required|numeric|digits:10',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'mobile.digits' => 'Mobile must be  10 digits.'
        ];
    }
}
